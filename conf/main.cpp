#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/sem.h>
#include <assert.h>

#define SIZE 96
#define KEY 11

using namespace std;

typedef struct MessageStruct
{
    long mtype;
    int num;
    char text[SIZE];
}message;

struct sembuf SEMLOCK = {0, -1, 0};

struct sembuf SEMUNLOCK = {0, 1, 0};

int in,ro;
int msqid;
int semid;

string HelpMessege="Инструкция.\n ./conf get - запрос конфигурации от демона configd. \n ./conf stop - остановка демонов. \n ./conf update - если во время работы демона был изменен ini файл хранимый в дериктории /root , то эта комманда обновит конфиг в памяти демона. \n ./conf set eth0 ipaddr XXX.XXX.XXX.XXX -изменит данное значение в памяти демона, и задаст вашему сетевому интерфейсу данный параметр.";

void SetId(key_t key)
{
    if ((msqid = msgget(key,0666)) < 0)
    {
           perror("msgget");
           exit(1);
    }
    if ((semid = semget(key,0,0666)) < 0)
    {
           perror("semget");
           exit(1);
    }
}

int main(int argc, char *argv[])
{
    if (argc<2)
    {
        cout<<HelpMessege<<endl;
        exit(0);
    }
    int me=1;  //Идентификационный номер сообщений от conf
    int configd=2;  //Идентификацмонный номер сообщений от демона configd
    key_t key=KEY;
    SetId(key);
    message mess;
    mess.mtype=configd;
    if(strcmp(argv[1],"get")==0)      //Получение конфига от демона configd
    {
        strcpy(mess.text,argv[1]);
        semop(semid,&SEMLOCK,1);
        msgsnd(msqid,&mess,SIZE,0);
        semop(semid,&SEMUNLOCK,1);
        msgrcv(msqid,&mess,SIZE,me,0);
        in=mess.num;
        cout<<"interfaces"<<endl;
        for (int i=0;i<=in-1;i++)       //получаем конфиги интерфейсов в количестве in;
        {
            msgrcv(msqid,&mess,SIZE,me,0);
            cout<<mess.text<<endl;
        }
        msgrcv(msqid,&mess,SIZE,me,0);
        ro=mess.num;
        cout<<"routes"<<endl;
        for (int i=0;i<=ro-1;i++)       //получаем конфиги путей в количестве ro;
        {
            msgrcv(msqid,&mess,SIZE,me,0);
            cout<<mess.text<<endl;
        }
    exit(0);
    }
    if(strcmp(argv[1],"stop")==0)       //Вырубаем демонов
    {
        strcpy(mess.text,argv[1]);
        semop(semid,&SEMLOCK,1);
        msgsnd(msqid,&mess,SIZE,0);
        semop(semid,&SEMUNLOCK,1);
        cout<<"daemons stoped"<<endl;
        exit(0);
    }
    if(strcmp(argv[1],"update")==0) //Обновление конфига из измененного файла
    {
        strcpy(mess.text,argv[1]);
        semop(semid,&SEMLOCK,1);
        msgsnd(msqid,&mess,SIZE,0);
        semop(semid,&SEMUNLOCK,1);
        cout<<"data must be updatet"<<endl;
        exit(0);
    }
    if(strcmp(argv[1],"set")==0)    //Отправка и применение параметров
    {
        if ((strlen(argv[2])<2)||(strlen(argv[2])>5))       //проверка
        {
            cout<<"Проверьте имя интерфейса."<<endl;
            exit(0);
        }
        if ((strlen(argv[3])<4)||(strlen(argv[3])>6))       //входных
        {
            cout<<"Проверьте имя параметра."<<endl;
            exit(0);
        }
        if ((strlen(argv[4])<6)||(strlen(argv[4])>15))      //параметров
        {
            cout<<"Недопустимое значение."<<endl;
            exit(0);
        }
        semop(semid,&SEMLOCK,1);
        strcpy(mess.text,argv[1]);  //посылаем команду
        msgsnd(msqid,&mess,SIZE,0);
        strcpy(mess.text,argv[2]);  //посылаем интерфейс или путь
        msgsnd(msqid,&mess,SIZE,0);
        strcpy(mess.text,argv[3]);  //посылаем параметр
        msgsnd(msqid,&mess,SIZE,0);
        strcpy(mess.text,argv[4]);  //посылаем значени
        msgsnd(msqid,&mess,SIZE,0);
        semop(semid,&SEMUNLOCK,1);
        msgrcv(msqid,&mess,SIZE,me,0);
        if (strcmp(mess.text,"OK")==0)
        {
            cout<<"Параметр установлен."<<endl;
        }
        else
        {
            cout<<"Что то пошло не так. Проверьте входные данные."<<endl;
        }
        msgrcv(msqid,&mess,SIZE,me,0);
        if (strcmp(mess.text,"ERROR!")==0)
        {
            cout<<"Ошибка. Проверьте входные данные."<<endl;
        }
        exit(0);
    }
    else
    {
        cout<<"Неверно введена комманда. Проверьте правильность ввода."<<endl;
        exit(1);
    }
}
