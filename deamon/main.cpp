#include <time.h>
#include <errno.h>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#define SIZE 96
#define KEY 11
#define INIFILEPATH "/root/cfg.ini"
#define LOCKFILEPATH "/var/run/configd.pid"
#define LOGFILEPATH "/var/log/configd.log"

using namespace std;

struct itemif       //структура интерфейса
{
    string IF;
    string IPADDR;
    string MASK;
};

struct itemroute        //структура пути
{
    string RT;
    string DEST;
    string NETMASK;
    string GATEWAY;
};

typedef struct MessageStruct
{
    long mtype;
    int num;
    char text[SIZE];
}message;

struct sembuf SEMLOCK = {0, -1, 0};

struct sembuf SEMUNLOCK = {0, 1, 0};

union semun {
  int val;
  struct semid_ds *buf;
  unsigned short *array;
  struct seminfo *__buf;
};

int s,in,ro;
message mess;
itemif inface[5];
itemroute route[10];

int conf=1;
int me=2;
int networkd=3;

int msqid;
int semid;

time_t rawtime;
struct tm * timeinfo;
char timebuf[50];

string HelpMessage="./deamon setup - создание ini файла в дериктории /root и запись в него примерной структуры. \n!!!ОБЯЗАТЕЛЬНО!!! После ./deamon setup обязательно забейте параметры в ini файл и удалите все комментарии !!!ОБЯЗАТЕЛЬНО!!! \n ./deamon start - запуск демона.";

void LogWrite(string event)
{
    ofstream file;
    time_t seconds = time(NULL);
    tm* timeinfo = localtime(&seconds);
    char* format = "%F %H:%M:%S";
    strftime(timebuf, 50, format, timeinfo);
    file.open(LOGFILEPATH,ios::app);
    file<<timebuf<<": "<<event<<endl;
    file.close();
}

void LoadCfg()      //функция загрузки конфига из ini файла
{
    int x=0;
    int y=0;
    string pathif="0.if";
    string pathip="0.ipaddr";
    string pathm="0.netmask";
    string pathdest="0.dest";
    string pathgate="0.gateway";
    string rt="route0";
    string buf;
    boost::property_tree::ptree pt;
    boost::property_tree::ini_parser::read_ini(INIFILEPATH, pt);
    s=pt.size()-1;
    for (int i=0;i<=s;i++)
    {
        char c='0'+i;
        char b='0'+x;
        rt[5]=b;
        pathif[0]=c;
        pathip[0]=c;
        pathm[0]=c;
        pathdest[0]=c;
        pathgate[0]=c;
        buf=pt.get<string>(pathif);
        if(buf==rt)
        {
            route[x].RT=rt;
            route[x].DEST=pt.get<string>(pathdest);
            route[x].NETMASK=pt.get<string>(pathm);
            route[x].GATEWAY=pt.get<string>(pathgate);
            x++;
        }
        else
        {
            inface[y].IF=pt.get<string>(pathif);
            inface[y].IPADDR=pt.get<string>(pathip);
            inface[y].MASK=pt.get<string>(pathm);
            y++;
        }
    }
}

bool Rights () // проверка наличия прав у программы (прова получаются через sudo или запуск через рута)
{
    string who="";
    who=getenv("USER");
    if (who!="root")
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

bool FileExists(const string& path)         //проверка наличия файла
{
        return ifstream(path.c_str()).good();
}

bool CheckDaemonActivity ()         //Проверка активности демона. есть нектороая корявость, но в идеале должно работать.
{
    int killRESULT;
    pid_t ifactPID;
    if(FileExists(LOCKFILEPATH)==0)
    {
        return 1;           //файла нет. ready to go!
    }
    ifstream ifrun;
    ifrun.open(LOCKFILEPATH);
    ifrun>>ifactPID;
    killRESULT=kill(ifactPID,0);        //попробуем убить
    if(killRESULT==0)
    {
        cout<<"Скорее всего демон активен, повторный запуск не возможен до остановки предидущего демона"<<endl;
        return 0;
    }
    else
    {
        if(errno==ESRCH)
        {
            cout<<"\n Процесс не активен, но файл блокировки занят.\n Удалите в ручную файл блокировки."<<endl;
            return 0;
        }
    }
    ifrun.close();
    return 1;
}

void AssembleIf(int i)      //сборка сообщения для отправки
{
    strcpy(mess.text,"");
    strcat(mess.text,inface[i].IF.c_str());
    strcat(mess.text," ipaddr: ");
    strcat(mess.text,inface[i].IPADDR.c_str());
    strcat(mess.text," netmask: ");
    strcat(mess.text,inface[i].MASK.c_str());
}

void AssembleIfTON(int i)      //сборка сообщения для отправки демону networkd
{
    strcpy(mess.text,"");
    strcat(mess.text,inface[i].IF.c_str());
    strcat(mess.text," ");
    strcat(mess.text,inface[i].IPADDR.c_str());
    strcat(mess.text," netmask ");
    strcat(mess.text,inface[i].MASK.c_str());
}

void AssembleRoute(int i)       //сборка сообщения для отправки
{
    strcpy(mess.text,"");
    strcat(mess.text,route[i].RT.c_str());
    strcat(mess.text," destenation: ");
    strcat(mess.text,route[i].DEST.c_str());
    strcat(mess.text," netmask: ");
    strcat(mess.text,route[i].NETMASK.c_str());
    strcat(mess.text," gateway: ");
    strcat(mess.text,route[i].GATEWAY.c_str());
}

void AssembleRouteTON(int i)       //сборка сообщения для отправки демону networkd
{
    strcpy(mess.text,"");
    strcat(mess.text,route[i].DEST.c_str());
    strcat(mess.text," netmask ");
    strcat(mess.text,route[i].NETMASK.c_str());
    strcat(mess.text," gw ");
    strcat(mess.text,route[i].GATEWAY.c_str());
}

bool DisAndApply(char what[16],char param[16],char value[16])  //поиск изменяемого параметра и применение изменений
{
    for (int i=0;i<=in;i++)
    {
        if(inface[i].IF==what)
        {
            if(strcmp(param,"ipaddr")==0)
            {
                inface[i].IPADDR=value;
                return 1;
            }
            if(strcmp(param,"mask")==0)
            {
                inface[i].MASK=value;
                return 1;
            }
        }
    }
    for (int i=0;i<=ro;i++)
    {
        if(route[i].RT==what)
        {
            if(strcmp(param,"dest")==0)
            {
                route[i].DEST=value;
                return 1;
            }
            if(strcmp(param,"gateway")==0)
            {
                route[i].GATEWAY=value;
                return 1;
            }
            if(strcmp(param,"mask")==0)
            {
                route[i].NETMASK=value;
                return 1;
            }
        }
    }
    return 0;
}

void MemClear()  //отчистка полей
{
    for (int i=0;i<=4;i++)
    {
        inface[i].IF="";
        inface[i].IPADDR="";
        inface[i].MASK="";
    }
    for (int i=0;i<=9;i++)
    {
        route[i].RT="";
        route[i].DEST="";
        route[i].NETMASK="";
        route[i].GATEWAY="";
    }
}

void Count()    //подсчет количества параметров
{
    in=0;
    ro=0;
    for(int i=0;i<=4;i++)
    {
        if(inface[i].IF!="")
        {
            in++;
        }
    }
    for(int i=0;i<=9;i++)
    {
        if(route[i].RT!="")
        {
            ro++;
        }
    }
}

void CreateExample() //создание примера ini файла
{
    ofstream inif;
    inif.open(INIFILEPATH,ios::app);
    inif<<"[0]      #Номер параметра, обязательно начинать с 0"<<endl;
    inif<<"if=  #здесь указывается имя интерфейса"<<endl;
    inif<<"ipaddr=  #здесь указывается ip адрес "<<endl;
    inif<<"netmask=     #здесь указывается маска"<<endl;
    inif<<"[1]"<<endl;
    inif<<"if=      #зесь помечается что данная структура является путем(route0,route1,route2, и т.д. начинать с route0)"<<endl;
    inif<<"dest=    #зесь указывается ip адрес азначения"<<endl;
    inif<<"netmask=     #здесь указывается маска"<<endl;
    inif<<"gateway=     #здесь указывается gateway"<<endl;
    inif<<"#!!!ОБЯЗАТЕЛЬНО!!!   ПОСЛЕ ВВЕДЕНИЯ ПАРАМЕТРОВ ОБЯЗАТЕЛЬНО УДАЛИТЕ ВСЕ КОММЕНТАРИИ   !!!ОБЯЗАТЕЛЬНО!!!"<<endl;
    inif.close();
}

void GetResponse() //ответ на запрос get от conf
{
    mess.mtype=conf;
    mess.num=in;
    strcpy(mess.text,"");
    semop(semid,&SEMLOCK,1);
    msgsnd(msqid,&mess,SIZE,0);
    for (int i=0;i<=in-1;i++)
    {
        AssembleIf(i);          //сбор параметров интерфейсов в сообщение и отправка
        msgsnd(msqid,&mess,SIZE,0);
    }
    mess.num=ro;
    msgsnd(msqid,&mess,SIZE,0);
    for (int i=0;i<=ro-1;i++)
    {
        AssembleRoute(i);       //сбор параметров путей в сообщение и отправка
        msgsnd(msqid,&mess,SIZE,0);
    }
    semop(semid,&SEMUNLOCK,1);
    LogWrite("conf get request");
}

void GettoNResponse() //ответ назапрос getTON от networkd
{
    mess.mtype=networkd;
    mess.num=in;
    strcpy(mess.text,"");
    semop(semid,&SEMLOCK,1);
    msgsnd(msqid,&mess,SIZE,0);
    for (int i=0;i<=in-1;i++)
    {
        AssembleIfTON(i);          //сбор параметров интерфейсов в сообщение и отправка
        msgsnd(msqid,&mess,SIZE,0);
    }
    mess.num=ro;
    msgsnd(msqid,&mess,SIZE,0);
    for (int i=0;i<=ro-1;i++)
    {
        AssembleRouteTON(i);       //сбор параметров путей в сообщение и отправка
        msgsnd(msqid,&mess,SIZE,0);
    }
    semop(semid,&SEMUNLOCK,1);
    LogWrite("networkd get or update request");
}

void Stop() //отправка запроса для networkd о завершении от configd и корректная остановка
{
    mess.mtype=networkd;
    strcpy(mess.text,"stop");
    semop(semid,&SEMLOCK,1);
    msgsnd(msqid,&mess,SIZE,0);
    semop(semid,&SEMUNLOCK,1);
    semctl(semid,0,IPC_RMID);
    msgctl(msqid,IPC_RMID,NULL);
    remove(LOCKFILEPATH);
    LogWrite("Stop");
    exit(0);
}

void SetResponse(char param[],char value[],char what[]) //установка параметров и отправка ответа conf
{
    msgrcv(msqid,&mess,SIZE,me,0);
    strcpy(what,mess.text);     //получаем интерфейс
    msgrcv(msqid,&mess,SIZE,me,0);
    strcpy(param,mess.text);    //получаем параметр
    msgrcv(msqid,&mess,SIZE,me,0);
    strcpy(value,mess.text);    //получаем значение
    if (DisAndApply(what,param,value)==1)
    {
        mess.mtype=networkd;
        strcpy(mess.text,"notify");
        semop(semid,&SEMLOCK,1);
        msgsnd(msqid,&mess,SIZE,0);
        for (int i=0;i<=in;i++)
        {
            if(inface[i].IF==what)
            {
                strcpy(mess.text,"interface");
                msgsnd(msqid,&mess,SIZE,0);
                AssembleIfTON(i);
                msgsnd(msqid,&mess,SIZE,0);
                LogWrite(mess.text);
            }
        }
        for (int i=0;i<=ro;i++)
        {
            if(route[i].RT==what)
            {
                strcpy(mess.text,"route");
                msgsnd(msqid,&mess,SIZE,0);
                AssembleRouteTON(i);
                msgsnd(msqid,&mess,SIZE,0);
                LogWrite(mess.text);
            }
        }
        mess.mtype=conf;
        strcpy(mess.text,"OK");
        msgsnd(msqid,&mess,SIZE,0);
        semop(semid,&SEMUNLOCK,1);
    }
    else
    {
        mess.mtype=conf;
        strcpy(mess.text,"NO");
        semop(semid,&SEMLOCK,1);
        msgsnd(msqid,&mess,SIZE,0);
        semop(semid,&SEMUNLOCK,1);
        LogWrite("set failure");
    }
}

void SetId(key_t key,int msgflg)
{
    if ((msqid = msgget(key, msgflg )) < 0)
    {
           perror("msgget");
           exit(1);
    }
    if ((semid = semget(key,1, msgflg )) < 0)
    {
           perror("semget");
           exit(1);
    }
    semctl(semid,0,SETVAL,1);
}

int main(int argc, char *argv[])
{
    if (argc<2)
    {
        cout<<HelpMessage<<endl;
        exit(0);
    }
    if (strcmp(argv[1],"setup")==0)
    {
        if (FileExists(INIFILEPATH)==0)
            {
                CreateExample();
            }
    }
    if (strcmp(argv[1],"start")==0)
    {
        pid_t PID;
        if (Rights()==0)
        {
            cout<<"Запустите приложение с правами администратора"<<endl;
            exit(-1);
        }
        cout<<"Запускаем configd"<<endl;
        chdir("/");
        PID=fork();
        if(PID==-1)
        {
            cout<<"Ошибка fork()\n"<<strerror(errno)<<endl;
        }
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
        if(PID==0)      //дочерний процесс(aka configd)
        {
            char what[16];
            char param[16];
            char value[16];
            key_t key=KEY;
            int msgflg = IPC_CREAT | 0666;
            if (CheckDaemonActivity()==1)       //созадем файл блокировки и записываем туда PID демона
            {
                ofstream deamon;
                deamon.open(LOCKFILEPATH);
                deamon<<getpid();
                deamon.close();
            }
            else
            {
                exit(-1);
            }
            SetId(key,msgflg);// Создаем очередь сообщений и семофор
            MemClear();     //отчищаем поля
            LoadCfg();      //загружаем конфиг из ini файла
            Count();        //и подсчитываем количество параметров
            umask(0);
            if (setsid()<0)     //с этого момета демон отрывается от терминала
            {
                cout<<"Ошибка setsid"<<endl;
            }
            close(STDIN_FILENO);
            close(STDOUT_FILENO);
            close(STDERR_FILENO);
            LogWrite("Start");
            for(;;)
            {
                msgrcv(msqid,&mess,SIZE,me,0);       //получение сообщения
                if(strcmp(mess.text,"gettoN")==0)
                {
                    GettoNResponse();
                }
                if(strcmp(mess.text,"get")==0)    //запрос от conf параметров
                {
                    GetResponse();
                }
                if(strcmp(mess.text,"stop")==0) //корректное отключени демона
                {
                    Stop();
                }
                if(strcmp(mess.text,"update")==0) // обновление параметров из ini файла
                {
                    MemClear();
                    LoadCfg();
                    Count();
                    mess.mtype=networkd;
                    strcpy(mess.text,"update");
                    msgsnd(msqid,&mess,SIZE,0);
                }
                if(strcmp(mess.text,"set")==0)        //изменение параметров
                {
                    SetResponse(param,value,what);
                }
            }
        }
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
        else        //Родительский процесс
        {
            exit(0);
        }
    }
    else
    {
        cout<<HelpMessage<<endl;
        exit(0);
    }
}
