#include <errno.h>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/sem.h>

#define SIZE 96
#define KEY 11
#define LOCKFILEPATH "/var/run/networkd.pid"
#define LOGFILEPATH "/var/log/networkd.log"

using namespace std;

typedef struct MessageStruct
{
    long mtype;
    int num;
    char text[SIZE];
}message;

struct sembuf SEMLOCK = {0, -1, 0};

struct sembuf SEMUNLOCK = {0, 1, 0};

message mess;
int msqid;
int semid;

int me=3;
int configd=2;
int conf=1;

char ifconfig[SIZE];
char routeadd[SIZE];

int in,ro;

int ans;

time_t rawtime;
struct tm * timeinfo;
char timebuf[50];

void LogWrite(string event)
{
    ofstream file;
    time_t seconds = time(NULL);
    tm* timeinfo = localtime(&seconds);
    char* format = "%F %H:%M:%S";
    strftime(timebuf, 50, format, timeinfo);
    file.open(LOGFILEPATH,ios::app);
    file<<timebuf<<": "<<event<<endl;
    file.close();
}


bool Rights () // Проверка наличия прав у программы (прова получаются через sudo или запуск через рута)
{
    string who="";
    who=getenv("USER");
    if (who!="root")
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

bool FileExists(const string& path)         //Проверка наличия файла
{
        return ifstream(path.c_str()).good();
}

bool CheckDaemonActivity ()         //Проверка активности демона. есть нектороая корявость, но в идеале должно работать.
{
    int killRESULT;
    pid_t ifactPID;
    if(FileExists(LOCKFILEPATH)==0)
    {
        return 1;           //файла нет. ready to go!
    }
    ifstream ifrun;
    ifrun.open(LOCKFILEPATH);
    ifrun>>ifactPID;
    killRESULT=kill(ifactPID,0);        //попробуем убить
    if(killRESULT==0)
    {
        cout<<"Скорее всего демон активен, повторный запуск не возможен до остановки предидущего демона"<<endl;
        return 0;
    }
    else
    {
        if(errno==ESRCH)
        {
            cout<<"\n Процесс не активен, но файл блокировки занят.\n Удалите в ручную файл блокировки."<<endl;
            return 0;
        }
    }
    ifrun.close();
    return 1;
}

void GetRequest() // запрос параметров от configd
{
    mess.mtype=configd;
    strcpy(mess.text,"gettoN"); //Отправка запроса configd
    semop(semid,&SEMLOCK,1);
    msgsnd(msqid,&mess,SIZE,0);
    semop(semid,&SEMUNLOCK,1);
    msgrcv(msqid,&mess,SIZE,me,0); //Получение параметров
    in=mess.num;
    for (int i=0;i<=in-1;i++)
    {
        strcpy(ifconfig,"ifconfig ");
        msgrcv(msqid,&mess,SIZE,me,0);
        strcat(ifconfig,mess.text);
        if (system(ifconfig)<0)
        {
            strcat(ifconfig," ERROR!");
            LogWrite(ifconfig);
        }
        else
        {
            LogWrite(ifconfig);
        }
    }
    msgrcv(msqid,&mess,SIZE,me,0);
    ro=mess.num;
    for (int i=0;i<=ro-1;i++)
    {
        strcpy(routeadd,"route add -net ");
        msgrcv(msqid,&mess,SIZE,me,0);
        strcat(routeadd,mess.text);
        if (system(routeadd)<0)
        {
            strcat(routeadd," ERROR!");
            LogWrite(routeadd);
        }
        else
        {
            LogWrite(routeadd);
        }
    }
}

void NotifyResponse() // реакция на notify
{
    msgrcv(msqid,&mess,SIZE,me,0);
    if(strcmp(mess.text,"interface")==0)
    {
        strcpy(ifconfig,"ifconfig ");
        msgrcv(msqid,&mess,SIZE,me,0);
        strcat(ifconfig,mess.text);
        if (system(ifconfig)<0)
        {
            strcat(ifconfig," ERROR!");
            LogWrite(ifconfig);
            mess.mtype=conf;
            strcpy(mess.text,"ERROR!");
            semop(semid,&SEMLOCK,1);
            msgsnd(msqid,&mess,SIZE,0);
            semop(semid,&SEMUNLOCK,1);
        }
        else
        {
            LogWrite(ifconfig);
            mess.mtype=conf;
            strcpy(mess.text,"Ok");
            semop(semid,&SEMLOCK,1);
            msgsnd(msqid,&mess,SIZE,0);
            semop(semid,&SEMUNLOCK,1);
        }
    }
    if(strcmp(mess.text,"route")==0)
    {
        strcpy(routeadd,"route add -net ");
        msgrcv(msqid,&mess,SIZE,me,0);
        strcat(routeadd,mess.text);
        if (system(routeadd)<0)
        {
            strcat(routeadd," ERROR!");
            LogWrite(routeadd);
            mess.mtype=conf;
            strcpy(mess.text,"ERROR!");
            semop(semid,&SEMLOCK,1);
            msgsnd(msqid,&mess,SIZE,0);
            semop(semid,&SEMUNLOCK,1);
        }
        else
        {
            LogWrite(routeadd);
            mess.mtype=conf;
            strcpy(mess.text,"Ok");
            semop(semid,&SEMLOCK,1);
            msgsnd(msqid,&mess,SIZE,0);
            semop(semid,&SEMUNLOCK,1);
        }
    }
}

void SetId(key_t key)
{
    if ((msqid = msgget(key,0666)) < 0)
    {
           perror("msgget");
           exit(1);
    }
    if ((semid = semget(key,0,0666)) < 0)
    {
           perror("semget");
           exit(1);
    }
}

int main()
{
    pid_t PID;
    if (Rights()==0)
    {
        cout<<"Запустите приложение с правами администратора"<<endl;
        exit(-1);
    }
    cout<<"Запускаем networkd."<<endl;
    chdir("/");
    PID=fork();
    if(PID==-1)
    {
        cout<<"Ошибка fork()\n"<<strerror(errno)<<endl;
    }
/*///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    if(PID==0)      //Дочерний процесс
    {
        key_t key=KEY;
        SetId(key); // получение дескрипоторв очереди сообщений и семофоров
        if (CheckDaemonActivity()==1)       //Создание файла блокировки
        {
            ofstream deamon;
            deamon.open(LOCKFILEPATH);
            deamon<<getpid();
            deamon.close();
        }
        else
        {
            exit(-1);
        }
        umask(0);
        if (setsid()<0)
        {
            cout<<"Ошибка setsid"<<endl;
        }
        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
        LogWrite("Start");
        GetRequest();
        for (;;)
        {
            msgrcv(msqid,&mess,SIZE,me,0);
            if(strcmp(mess.text,"notify")==0) //Обновление конфигурации
            {
                NotifyResponse();
            }
            if(strcmp(mess.text,"stop")==0) //Кореектная остановка демона
            {
                remove(LOCKFILEPATH);
                LogWrite("Stop");
                exit(0);
            }
            if(strcmp(mess.text,"update")==0) //Обновление параметров, с измененного INI файла
            {
                GetRequest();
            }
        }
    }
/*///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    else        //Родительский процесс
    {
        exit(0);
    }
}
